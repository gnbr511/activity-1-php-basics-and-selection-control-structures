<?php require_once './code.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 1 - PHP Basics and Selection Control Structures</title>
</head>
<body>
	<h1>Full Address</h1>
	<p><?php echo getFullAddress("Brgy. 123", "Maasin City", "Leyte", "Philippines") ?></p>

	<p><?php echo getFullAddress("Brgy. 456", "Dulag", "Leyte", "Philippines") ?></p>

	<h1>Letter-Based Grading</h1>
	<p><?php echo getLetterGrade(87) ?></p>
	<p><?php echo getLetterGrade(94) ?></p>
	<p><?php echo getLetterGrade(74) ?></p>
</body>
</html>